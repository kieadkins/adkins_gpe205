﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneratir : MonoBehaviour
{
    // Variables
    private GameManager instance;
    public int mapSeed;
           
    public int rows;
    public int cols;

    public float roomWidth = 50.0f;
    public float roomHeight = 50.0f;

    private GameObject[,] map;
    public GameObject[] tilePrefabs;
    public int[] degreeRotate = new int[] { 0, 90, 180, 270 };

    public Transform chosenSpawn;
    public GameObject playerTank;
    public GameObject player2Tank;
    public GameObject singlePlayerTank;

    private void Start()
    {
        instance = GetComponent<GameManager>();

        //Checking if it is the map of the day
        if (instance.saveManager.mapOfTheDay == 1)
        {
            mapSeed = DateToInt(DateTime.Now.Date);
        }

        // If the mapseed is 0
        if (mapSeed != 0)
        {
            // Set our seed
            UnityEngine.Random.seed = mapSeed;
        }

        // Randomly generate a map
        else
        {
           
            // Set our seed
            UnityEngine.Random.seed = DateToInt(DateTime.Now);
        }

        // Generate Map
        GenerateMap();   
    }

    // Setting the random generator seed
    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    // Randomly generating a room
    public GameObject RandomRoomPrefab()
    {
        return tilePrefabs[UnityEngine.Random.Range(0, tilePrefabs.Length)];
    }

    // Generating the map
    public void GenerateMap()
    {      
        // Clearing the grid
        map = new GameObject[cols, rows];       

        // For each row
        for (int i = 0; i < rows; i++)
        {
            // For each column
            for (int j = 0; j < cols; j++)
            {
                // Instantiate room
                GameObject temp = Instantiate(RandomRoomPrefab());

                // Hold Room
                map[j, i] = temp;

                // Moving the tiles
                temp.transform.position = new Vector3(j * roomWidth, 0, -i * roomHeight);

                // Create new grid
                RoomScript room = temp.GetComponent<RoomScript>();
               
                // Parenting new room
                temp.transform.parent = this.transform;

                // Renaming object
                temp.name = "Room_" + j + "," + i;

                // Open the door based on place in rows
                if (i == 0)
                {
                    room.doorSouth.SetActive(false);
                }

                else if (i==rows-1)
                {
                    room.doorNorth.SetActive(false);
                }

                else
                {
                    room.doorNorth.SetActive(false);
                    room.doorSouth.SetActive(false);
                }

                // Opening doors based on place in columns
                if (j==0)
                {
                    room.doorWest.SetActive(false);
                }

                else if (j == cols-1)
                {
                    room.doorEast.SetActive(false);
                }

                else
                {
                    room.doorEast.SetActive(false);
                    room.doorWest.SetActive(false);
                }

                // Rotating the terrain randomly
                room.terrain.transform.Rotate(0.0f, degreeRotate[UnityEngine.Random.Range(0, degreeRotate.Length)], 0.0f);

                // Pushing waypoints to game manager
                foreach (Transform waypoint in room.waypoint)
                {
                    instance.waypointList.Add(waypoint);
                }

                // Pushing spawn points to game manager                
               instance.playerSpawnList.Add(room.playerSpawn);               

                // Save to grid array
                map[j, i] = temp;
                
                // Instantiate AI
                Instantiate(instance.ai[UnityEngine.Random.Range(0, instance.ai.Length)], room.spawnPoint);
                  

                // Instantiate Pickups
                if (instance.powerUps != null)
                {
                    Instantiate(instance.powerUps[UnityEngine.Random.Range(0, instance.powerUps.Length)], room.powerSpawn[UnityEngine.Random.Range(0, room.powerSpawn.Length)]);
                }

            }
        }

        // Choosing a spawn point
        Transform chosenSpawn = instance.playerSpawnList[UnityEngine.Random.Range(0, instance.playerSpawnList.Count - 1)];

        if (instance.saveManager.singlePlayer == 2)
        {
            instance.playerSpawnList.Remove(chosenSpawn);

            Transform player2Spawn = instance.playerSpawnList[UnityEngine.Random.Range(0, instance.playerSpawnList.Count - 1)];
        
            if (instance.playPresent == false)
            {
            // Instantiate player
            playerTank = Instantiate(instance.player, chosenSpawn) as GameObject;
            instance.playPresent = false;

            instance.pc = playerTank.GetComponent<PlayerController>();
            }

        
            if (instance.play2Present == false)
            {
                player2Tank = Instantiate(instance.player2, player2Spawn) as GameObject;
                instance.play2Present = true;

                instance.pc2 = player2Tank.GetComponent<PlayerController>();
            }
        }

        else
        {
            if (instance.playPresent == false)
            {
                // Instantiate player
                singlePlayerTank = Instantiate(instance.singlePlayer, chosenSpawn) as GameObject;
                instance.playPresent = false;

                instance.pc = singlePlayerTank.GetComponent<PlayerController>();
            }
        }
    }
}
