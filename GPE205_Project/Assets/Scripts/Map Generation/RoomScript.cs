﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomScript : MonoBehaviour
{
    public int roomNum;
    public GameObject doorNorth;
    public GameObject doorSouth;
    public GameObject doorEast;
    public GameObject doorWest;

    public GameObject terrain;
    public Transform spawnPoint;
    public Transform playerSpawn;
    public Transform[] powerSpawn;
    public Transform[] waypoint = new Transform[3];
}
