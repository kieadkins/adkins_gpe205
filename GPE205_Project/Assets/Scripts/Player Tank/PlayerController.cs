﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{    
    private GameManager instance;
    public TankData data;
    public TankShoot shoot;
    public Canvas canvas;

    // UI Components
    public Text highScoreText;
    public Text scoreText;
    public Image healthImage;
    public GameObject endPanel;

    // Start is called before the first frame update
    void Start()
    {
        instance = GameManager.instance;
        canvas = GetComponent<Canvas>();

        // Adding to list
        instance.players.Add(this.gameObject);
    }


    private void Update()
    {
        SetScore();

    }


    // Setting the score
    public void SetScore()
    {
       scoreText.text = "Score: " + data.tankScore.ToString();
        
    }

    void SetHealth()
    {
        healthImage.fillAmount = data.currentHealth / 100;

        if (data.currentHealth == 0)
        {
            OnDestroy();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            data.currentHealth -= data.bulletDamage;
            
            // Setting Health
            SetHealth();

            // Destroying the bullet
            Destroy(collision.gameObject);
        }        
    }

    private void OnDestroy()
    {
        // Remove from list
        instance.players.Remove(this.gameObject);

        // End Game Screen
        endPanel.SetActive(true);

        // Set High Score
        instance.saveManager.Save();
    }

}
