﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankEngine : MonoBehaviour
{
    // Declaring the variables
    private CharacterController charControl;
    private TankData data;
    public Transform tf;
    public Transform tankTurret;


    // Start is called before the first frame update
    void Start()
    {
        // Getting components
        charControl = gameObject.GetComponent<CharacterController>();
        tf = gameObject.GetComponent<Transform>();
        data = GetComponent<TankData>();
    }
    
    // Moving the tank            
    public void TankMove( float speed )
    {
        // Storing the vector
        Vector3 speedVec = tf.forward * speed * Time.deltaTime;
        charControl.SimpleMove(speedVec);
    }

    // Turning the tank
    public void TankTurn( float speed )
    {
        // Storing the vector
        Vector3 rotateVec = Vector3.up * speed * Time.deltaTime;
        tf.Rotate(rotateVec, Space.Self);
    }

    // Rotating towards a given point
    public bool RotateTowards(Vector3 target, float speed)
    {
        if (tf != null)
        {
            Vector3 targetDir;

            targetDir = target - tf.position;

            Quaternion targetRotation = Quaternion.LookRotation(targetDir);

            tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);

            // Returning false
            if (targetRotation == tf.rotation)
            {
                return false;
            }
        }

        return true;
    }

    public void TurretTurn(float rotateSpeed)
    {
        Vector3 turretRotate = Vector3.up * rotateSpeed * Time.deltaTime;
        tankTurret.Rotate(turretRotate, Space.Self);
    }
}
