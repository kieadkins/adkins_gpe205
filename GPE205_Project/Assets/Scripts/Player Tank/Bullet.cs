﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletTimeOut = 0.5f;
    public ParticleSystem explode;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, bulletTimeOut);
    }

    private void OnCollisionEnter(Collision collision)
    {      

        if (collision.gameObject.tag == "Enemy")
        {
            // Destroying the enemy
            Destroy(this.gameObject);
        }

        else
        {
            // Destroy the bullet
            Destroy(gameObject);

        }
    }

    private void OnDestroy()
    {
        Invoke("Explode", 0);

    }

    void Explode()
    {
        ParticleSystem ps = Instantiate(explode, transform.position, Quaternion.identity) as ParticleSystem;

        gameObject.GetComponent<MeshRenderer>().enabled = false;
        Destroy(gameObject, ps.duration);
    }
}
