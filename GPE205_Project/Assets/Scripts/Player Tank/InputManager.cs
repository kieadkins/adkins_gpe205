﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Declaring the variables
    public enum CustomInput {  WASD, arrowKeys };
    public CustomInput input = CustomInput.WASD;
    public TankEngine engine;
    public TankData data;

    // Update is called once per frame
    void Update()
    {
        switch (input)
        {
            case CustomInput.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    engine.TankMove(data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.S))
                {
                    engine.TankMove(-data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.D))
                {
                    engine.TankTurn(data.turnSpeed);
                }

                if (Input.GetKey(KeyCode.A))
                {
                    engine.TankTurn(-data.turnSpeed);
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    data.shoot.Shoot();
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    engine.TurretTurn(-data.turretTurnSpeed);
                }

                if (Input.GetKey(KeyCode.E))
                {
                    engine.TurretTurn(data.turretTurnSpeed);
                }

                break;

            case CustomInput.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    engine.TankMove(data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    engine.TankMove(-data.moveSpeed);
                }

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    engine.TankTurn(data.turnSpeed);
                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    engine.TankTurn(-data.turnSpeed);
                }

                if (Input.GetKeyDown(KeyCode.RightShift))
                {
                    data.shoot.Shoot();
                }

                if (Input.GetKey(KeyCode.N))
                {
                    engine.TurretTurn(-data.turretTurnSpeed);
                }

                if (Input.GetKey(KeyCode.M))
                {
                    engine.TurretTurn(data.turretTurnSpeed);
                }

                break;
        }
    }
}
