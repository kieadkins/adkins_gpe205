﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour
{
    // Declaring the variables
    public float secondsBetweenShot = 0.5f;
    private float currentTime = 0.0f;
    public float fireRate = 0.5f;

    public float shellForce;
    public Transform shootSpawn; 

    public Rigidbody bullet;
    private AudioSource mainAudio;
    public AudioClip fireAudio;

    private void Start()
    {
        mainAudio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
       
    }

    // Shooting the bullet
    public void Shoot()
    {
        if (currentTime >= secondsBetweenShot)
        {
            Rigidbody bulletInst = Instantiate(bullet, shootSpawn.position, shootSpawn.rotation);

            bulletInst.velocity = shootSpawn.TransformDirection(Vector3.forward * shellForce);
            
            currentTime = 0f;

            // Playing the sound
            mainAudio.PlayOneShot(fireAudio);
        }
    }
}
