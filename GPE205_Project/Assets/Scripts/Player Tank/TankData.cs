﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    // Declaring the variables
    public float moveSpeed = 300f; 
    public float turnSpeed = 180f;
    public float turretTurnSpeed = 100f;
    public int maxHealth = 100;
    public float currentHealth;
    public int bulletDamage;
    //public Transform target;

    // Player Score
    public int tankScore; 
    public int pointToDestroy;

    // Noise Information
    public float volume = 0;
    public float maxNoiseDis = 10;
    [HideInInspector]public Transform tf;
    
    [HideInInspector]public TankShoot shoot;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        shoot = GetComponent<TankShoot>();
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        // If health is below zero
        if (currentHealth < 0)
        {
            OnDestroy();
        }
    }    

    private void OnDestroy()
    {
        // Destroying the gameobject
        Destroy(gameObject);
    }
}
