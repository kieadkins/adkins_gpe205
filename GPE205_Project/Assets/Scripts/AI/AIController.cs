﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AIController : MonoBehaviour
{
    private GameManager instance;
    private TankShoot shoot;
    private TankData data;
    private Transform tf;
    private TankEngine engine;

    public List<Transform> waypoints;
    private GameObject playTarget;
    private RoomScript room;

    // Health
    public Slider hSlider;
    public Image fillImage;
    public Color fullHealth = Color.green;
    public Color zeroHealth = Color.red;

    // Waypoint Info
    public int currentWaypoint = 0;
    public float closeEnough = 1.0f;
    public enum LoopType { Stop, Loop, PingPong};
    public LoopType loopType;
    public bool isPatrol = true;

    // Chase Variables
    public enum AIState { Patrolling, Aggressive, Cowardly, Stationary};
    public AIState aiState = AIState.Stationary;
    public float stateEnterTime;
    public float aiSenseRadius;
    public float restingHealRate;

    //  Flee Variables
    public float fleeDistance = 1.0f;

    // Obstacle Avoidance
    private int avoidanceStage = 0;
    public float avoidanceTime = 2.0f;
    public int raycastMax = 5;
    private float exitTime;
       
    // View Info
    public float maxViewAngle;
    public float maxDistance;
    public bool inBounds = false;

    // Audio and Particle
    private AudioSource mainAudio;
    public AudioClip deathSound;
    public GameObject destroyPartPrefab;
    public ParticleSystem destroyPart;
    private float puffTime = 0;

    public GameObject targetTank;

    private void Awake()
    {
        destroyPart = Instantiate(destroyPartPrefab).GetComponent<ParticleSystem>();

        destroyPart.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        engine = GetComponent<TankEngine>();
        instance = GameManager.instance;
        data = GetComponent<TankData>();
        shoot = GetComponent<TankShoot>();
        mainAudio = GetComponent<AudioSource>();

        // Adding waypoints to tank
        waypoints.AddRange(instance.waypointList);

        data.currentHealth = data.maxHealth;

        SetHealthUI();

        targetTank = instance.player;
        Debug.Log(targetTank);
    }

    // Update is called once per frame
    void Update()
    {
        Personality();
    }

    public void Personality()
    {
        switch (aiState)
        {
            case AIState.Patrolling:

                if (FieldOfView(targetTank) == true)
                {
                    Vector3 lastPosition = targetTank.transform.position;

                    // Rotate towards
                    engine.RotateTowards(lastPosition, data.turnSpeed);

                    // Shoot
                    shoot.Shoot();
                }

                else
                {
                    WaypointMovement();

                    // Heal
                    DoRest();
                }

                break;

            case AIState.Aggressive:

                if (FieldOfView(targetTank) == true)
                {
                    // Chasing
                    Chase(targetTank);

                    shoot.Shoot();
                }

                else
                {
                    //Heal
                    DoRest();
                }

                break;

            case AIState.Cowardly:

                //Hearing
                if (Hearing(targetTank))
                {
                    Flee(targetTank);
                }

                else
                {
                    //Heal
                    DoRest();
                }

                break;

            case AIState.Stationary:

                if (FieldOfView(targetTank))
                {
                    shoot.Shoot();
                }
                else
                {
                    // Heal
                    DoRest();
                }

                break;

        }
    }
      
    //Setting the AI to move along waypoints
    public void WaypointMovement()
    {
        if (engine.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed * Time.deltaTime))
        {
            // Do nothing!
            if (CanMove(data.moveSpeed * Time.deltaTime) == true)
            {
                // Move forward
                engine.TankMove(data.moveSpeed * Time.deltaTime);
            }
        }
        else
        {
            if (CanMove(data.moveSpeed * Time.deltaTime) == true)
            {
                // Move forward
                engine.TankMove(data.moveSpeed * Time.deltaTime);
            }

            else
            {
                avoidanceStage = 1;

                Avoidance();
            }
        }

        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            // If enum is set to stop
            if (loopType == LoopType.Stop)
            {
                if (currentWaypoint < waypoints.Count - 1)
                {
                    // Advance to the next waypoint
                    currentWaypoint++;
                }
            }

            // If enum is set to loop
            else if (loopType == LoopType.Loop)
            {
                if (currentWaypoint < waypoints.Count - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }

            else if (loopType == LoopType.PingPong)
            {

                // If AI is patrolling
                if (isPatrol)
                {
                    if (currentWaypoint < waypoints.Count - 1)
                    {
                        currentWaypoint++;
                    }

                    else
                    {
                        isPatrol = false;
                        currentWaypoint--;
                    }
                }

                // Otherwise
                else
                {
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }

                    else
                    {
                        isPatrol = true;
                        currentWaypoint++;
                    }
                }
            }
        }
    }

    // Checking field of view
    public bool FieldOfView(GameObject target)
    {
        if (inBounds == true)
        {
            if (target != null)
            {
                // Finding the distance between target and current
                Vector3 dist = target.transform.position - tf.position;

                // Finding angle between target and current
                float angle = Vector3.Angle(dist, tf.forward);

                if (angle < maxViewAngle)
                {
                    return true;
                }

            }
        }
            return false;        
    }

    // Hearing
    public bool Hearing(GameObject target)
    {
        if (inBounds == true)
        {
            if (target != null)
            {
                float distance = Vector3.Distance(tf.position, target.transform.position);

                // If the tank has no volume
                if (instance.pc.data.volume < 0)
                {
                    return false;
                }

                // If the distance is greater than the max noise distance
                else if (distance > data.maxNoiseDis)
                {
                    return false;
                }

                return true;
            }
        }

        return false;
    }

    // Checking for movement
    public bool CanMove (float speed)
    {
        RaycastHit hit;
        Debug.DrawRay(tf.position, tf.forward * raycastMax, Color.yellow);


        // If the raycast hits somethings
        if (Physics.Raycast (tf.forward, tf.forward, out hit, raycastMax))
        {
            if (hit.collider.tag != "Player")
            {
                return false;
            }
        }
        
        return true;
    }

    // Avoiding obstacles
    public void Avoidance()
    {
        // Avoidance stage 1
        if (avoidanceStage == 1)
        {
            engine.TankTurn(-1 * data.turnSpeed);

            // If we can move go to stage 2
            if (CanMove (data.moveSpeed))
            {
                avoidanceStage = 2;
            }

            exitTime = avoidanceTime;
        }

        // Avoidance stage 2
        else if (avoidanceStage == 2)
        {
            if (CanMove(data.moveSpeed))
            {
                exitTime -= Time.deltaTime;

                engine.TankMove(data.moveSpeed);

                // Return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }

                // Since we can't move forward go to stage 1
                else
                {
                    avoidanceStage = 1;
                }
            }
        }

    }

    // Healing the tank in rest
    public void DoRest()
    {
        data.currentHealth += restingHealRate * Time.deltaTime;

        data.currentHealth = Mathf.Min(data.currentHealth, data.maxHealth);
    }

    // Setting the AI to Flee from the Player
    public void Flee(GameObject target)
    {
        Vector3 vectorToTarget = target.transform.position - tf.position;

        Vector3 vectorAwayFromTarget = -5 * vectorToTarget;

        //vectorAwayFromTarget.Normalize();

        vectorAwayFromTarget *= fleeDistance;

        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        engine.RotateTowards(fleePosition, data.turnSpeed);

        // Checking to see if we can move
        if (CanMove(data.moveSpeed))
        {
            engine.TankMove(data.moveSpeed);
        }

        // Otherwise
        else
        {
            avoidanceStage = 1;
        }
    }

    // Setting the AI to Chase the Player
    public void Chase(GameObject target)
    {
        engine.RotateTowards(target.transform.position, data.turnSpeed);

        // Checking to see if we can move
        if (CanMove(data.moveSpeed))
        {
            engine.TankMove(data.moveSpeed);
        }

        // Otherwise
        else
        {
            avoidanceStage = 1;
        }
    }

    // Setting Health UI
    public void SetHealthUI()
    {
        hSlider.value = data.currentHealth;

        fillImage.color = Color.Lerp(zeroHealth, fullHealth, data.currentHealth / data.maxHealth);
    }

    // When Destroyed
    private void OnDestroy()
    {
        if (destroyPart != null)
        {
            destroyPart.transform.position = transform.position;
            destroyPart.gameObject.SetActive(true);
            destroyPart.Play();

            mainAudio.PlayOneShot(deathSound);
            gameObject.SetActive(false);
        }
        
    }
    
    // On Collision
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            // Adding Damage
            data.currentHealth -= data.bulletDamage;

            // Destroy bullet
            Destroy(collision.gameObject);

            if (data.currentHealth == 0)
            {
                // Adding tank destruction points
                instance.pc.data.tankScore += data.pointToDestroy;
                
                // Destroying the object
                Destroy(gameObject);
            }

            SetHealthUI();
        }

        else if (collision.gameObject.tag == "Player2Bullet")
        {
            // Adding Damage
            data.currentHealth -= data.bulletDamage;

            // Destroy bullet
            Destroy(collision.gameObject);

            if (data.currentHealth == 0)
            {
                // Adding tank destruction points
                instance.pc2.data.tankScore += data.pointToDestroy;
                
                // Destroying the object
                Destroy(gameObject);
            }

            SetHealthUI();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            inBounds = true;           
        }

        else
        {
            inBounds = false;
        }
    }
}
