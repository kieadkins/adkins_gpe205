﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankPersonality : MonoBehaviour
{
//Patrolling - 
//This enemy tank follows the designer set waypoints.However, when the tank hears the player it turns and fires towards the player.

//Aggressive -
//The aggressive tank stays stationary until it "sees" the player, then it pursues while shooting.

//Cowardly - 
//This enemy tank stays stationary until it hears the player. Once the player comes into its view, the tank flees until it is out of hearing radius.

//Stationary -
//The stationary tank follows waypoints, but when it "sees" the player, it shoots. However, the shot is not aligned toward the player.
}
