﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDirectionControl : MonoBehaviour
{
    public bool relativeRotation = true;
    private Quaternion relRotation;
    
    // Start is called before the first frame update
    void Start()
    {
        relRotation = transform.parent.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (relativeRotation)
        {
            transform.rotation = relRotation;
        }
    }
}
