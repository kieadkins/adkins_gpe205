﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public GameManager instance;

    // If single player is 1 == true, else false
    public int singlePlayer = 1;

    // If mapOfTheDay is 1 == true, else false
    public int mapOfTheDay = 0;
    public int randomMap = 0;


    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        Load();
    }

    public void Save()
    {
        if (instance != null)
        {
            PlayerPrefs.SetInt("HighScore", instance.highScore);
        }

        PlayerPrefs.SetInt("singlePlayer", singlePlayer);
        PlayerPrefs.SetInt("mapOfTheDay", mapOfTheDay);
        PlayerPrefs.SetInt("RandomMap", randomMap);

        PlayerPrefs.Save();
    }

    public void Load()
    {
        if (instance != null)
        {
            instance.highScore = PlayerPrefs.GetInt("HighScore");
        }
        singlePlayer = PlayerPrefs.GetInt("singlePlayer");
        mapOfTheDay = PlayerPrefs.GetInt("mapOfTheDay");
        randomMap = PlayerPrefs.GetInt("RandomMap");
    }

    public void OnLevelWasLoaded(int level)
    {
        instance = (GameManager)FindObjectOfType(typeof(GameManager));        
    }
}
