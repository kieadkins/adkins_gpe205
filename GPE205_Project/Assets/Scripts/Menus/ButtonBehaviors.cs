﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonBehaviors : MonoBehaviour
{
    public SaveManager saveManager;

    private void Start()
    {
        saveManager = (SaveManager)FindObjectOfType(typeof(SaveManager));
    }

    public void LoadLevel()
    {
        saveManager.Save();
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        saveManager.Save();
        Application.Quit();
    }    
    
}
