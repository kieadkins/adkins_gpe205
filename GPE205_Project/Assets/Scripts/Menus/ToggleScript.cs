﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleScript : MonoBehaviour
{
    public SaveManager saveManager;

    public void DailyMap()
    {
        if (saveManager.mapOfTheDay == 0)
        {
            saveManager.mapOfTheDay = 1;
        }

        else
        {
            saveManager.mapOfTheDay = 0;
        }
    }

    public void RandomMap()
    {
        if (saveManager.randomMap == 0)
        {
            saveManager.randomMap = 1;
        }

        else
        {
            saveManager.randomMap = 0;
        }
    }

    public void SetPlayer(int playerNum)
    {
        if (playerNum == 1)
        {
            saveManager.singlePlayer = 1;
        }

        else
        {
            saveManager.singlePlayer = 2;
        }
    }
}
