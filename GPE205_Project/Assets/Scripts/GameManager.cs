﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    // Declaring the variables
    public static GameManager instance;
    public SaveManager saveManager;
    public MapGeneratir mapGen;
    public GameObject[] ai;
    public AIController aiCon;
    public GameObject player;
    public List<GameObject> players;
    public PlayerController pc;
    public PlayerController pc2;
    public GameObject player2;
    public GameObject singlePlayer;
    public PlayerController singlePlayerCon;
    public GameObject finalPanel;

    public GameObject[] powerUps;
    public float powerSpawnTime;

    public RoomScript room;
    public List<Transform> waypointList;
    public List<Transform> playerSpawnList;
    public bool playPresent = false;
    public bool play2Present = false;

    public int highScore = 0;

    // On Awake
    private void Awake()
    {                
        if (instance == null)
        {
            instance = this;
        }

        else
        {
            Debug.LogError("There cannot be duplicate game managers\n");
            Destroy(gameObject);
        }
        
        room = instance.GetComponent<RoomScript>();
        mapGen = GetComponent<MapGeneratir>();
        saveManager = (SaveManager)FindObjectOfType(typeof(SaveManager));

        DontDestroyOnLoad(this.gameObject);

        
    }

    public void Start()
    {
        saveManager.Load();
    }

    void Update()
    {
        // Setting high score
        if (pc != null && pc2 != null)
        {
            if (highScore <= pc.data.tankScore)
            {
                highScore = pc.data.tankScore;

                SetHighScore();
            }

            if (highScore <= pc2.data.tankScore)
            {
                highScore = pc2.data.tankScore;

                SetHighScore();
            }

            else
            {
                SetHighScore();
            }
        }

        // Setting Death
        if (players.Count == 0)
        {
            finalPanel.SetActive(true);
        }        
    }

    // Setting the score
    public void SetHighScore()
    {
       pc.highScoreText.text = "High Score: " + highScore.ToString();

        if (pc2 != null)
        {
            pc2.highScoreText.text = "High Score: " + highScore.ToString();
        }
    }

}
