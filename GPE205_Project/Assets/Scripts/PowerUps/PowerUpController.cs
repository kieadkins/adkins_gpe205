﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public List<PowerUp> powerups;
    private TankData data;

    private void Start()
    {
        powerups = new List<PowerUp>();

        data = GetComponent<TankData>();
    }

    private void Update()
    {
        // Expired powerups
        List<PowerUp> expiredPowerups = new List<PowerUp>();

        foreach (PowerUp power in powerups)
        {
            // Subtracting from the timer
            power.duration -= Time.deltaTime;
            
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }

        foreach (PowerUp power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }

        expiredPowerups.Clear();
    }

    // Add function
    public void Add (PowerUp powerup)
    {
        // Add powerup
        powerup.OnActivate(data);
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }

}
