﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControl : MonoBehaviour
{
    private GameManager instance;
    public bool childEmpty = true;
    public bool canChange;
    public float timeSpawn = 5f;
    public float currentTime;

    public void Start()
    {
        instance = GameManager.instance;
        currentTime = timeSpawn;

        if (transform.childCount == 0)
        {
            childEmpty = true;
            canChange = false;
        }

        else
        {
            childEmpty = false;
            canChange = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount == 0)
        {
            childEmpty = true;
        }

        if (canChange == true && childEmpty == true)
        {
            // Start subtracting
            currentTime -= Time.deltaTime;

            if (currentTime <= 0)
            {
                Instantiate(instance.powerUps[UnityEngine.Random.Range(0, instance.powerUps.Length)], transform);

                childEmpty = false;
                currentTime = timeSpawn;
            }
        }
        
    }
}
