﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    public GameObject pickupPrefab;
    public float spawnDelay;
    public float nextSpawnTime;
    private Transform tf;
    private object spawnedPickup;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnedPickup == null)
        {
            // If it is time to spawn
            if (Time.time > nextSpawnTime)
            {
                spawnedPickup = Instantiate(pickupPrefab, tf.position, Quaternion.identity) as GameObject;
                nextSpawnTime = Time.time + spawnDelay;
            }

            else
            {
                nextSpawnTime = Time.time + spawnDelay;
            }

        }
    }
}
