﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    // Variables
    public float speedMod;
    public float healthMod;
    public float fireRateMod;
    public float shotSecondMod;

    public float duration;
    public bool isPermanent;

    public void OnActivate(TankData target)
    {
        if (target != null)
        {
            target.moveSpeed += speedMod;
            target.currentHealth += healthMod;
            target.shoot.fireRate += fireRateMod;
            target.shoot.secondsBetweenShot += shotSecondMod;
        }
    }

    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedMod;
        target.currentHealth -= healthMod;
        target.shoot.fireRate -= fireRateMod;
        target.shoot.secondsBetweenShot -= shotSecondMod;
    }
}
