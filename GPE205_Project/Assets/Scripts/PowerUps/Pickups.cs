﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickups : MonoBehaviour
{
    // Variables
    public ParticleSystem destroyPart;
    public PowerUp powerup;
    public AudioClip feedback;
    private float puffTime = 0;
    

    public void OnTriggerEnter(Collider other)
    {
        // If it is run over by the player
        if (other.gameObject.tag == "Player")
        {
            // Find power controller
            PowerUpController powCon = other.gameObject.GetComponent<PowerUpController>();
            
            if (powCon != null)
            {
                // Add to list
                powCon.Add(powerup);

                if (feedback != null)
                {
                    AudioSource.PlayClipAtPoint(feedback, transform.position, 1.0f);
                }               
            }

            // On Destroy
            OnDestroy();
        }

        // If it is run over by an enemy
        if (other.gameObject.tag == "Enemy")
        {
            // Find power controller
            PowerUpController powCon = other.gameObject.GetComponent<PowerUpController>();

            if (powCon != null)
            {
                // Add to list
                powCon.Add(powerup);

                if (feedback != null)
                {
                    AudioSource.PlayClipAtPoint(feedback, transform.position, 1.0f);
                }
            }

            // On Destroy
            OnDestroy();
        }
        
    }

    private void OnDestroy()
    {
        Invoke("Puff", puffTime);
        
    }

    void Puff()
    {
        ParticleSystem ps = Instantiate(destroyPart, transform.position, Quaternion.identity) as ParticleSystem;

        gameObject.GetComponent<MeshRenderer>().enabled = false;
        Destroy(gameObject, ps.duration);
    }
}
